FROM nginx:1.20-alpine
COPY build /usr/share/nginx/html
EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]

